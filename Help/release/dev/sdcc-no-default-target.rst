sdcc-no-default-target
----------------------

* The default flags used for SDCC no longer include any target-specific flags.
  Previously the default flags were hard-coded for 8051.
